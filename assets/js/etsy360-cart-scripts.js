/**
 * Created by viviosoft on 17/12/14.
 */

jQuery(function () {


    // Show the pop-up cart AFTER the page has loaded
    jQuery('.e360-pop-window').show();

    jQuery('#e360-pop-windowclose').on('click', function () {
        //console.log('CLICKED!');

        if (jQuery('.e360-pop-window').hasClass('down')) {
            jQuery('.e360-pop-window').removeClass('down');
            jQuery('.e360-pop-windowclose').removeClass('down');
        } else {
            jQuery('.e360-pop-windowclose').addClass('down');
            jQuery('.e360-pop-window').addClass('down');
        }


        //jQuery(".e360-pop-window").css("bottom", "-200px");
    });


    jQuery('select').change(this, function(){

        var addToCart = jQuery('.add-to-cart');
        var selectVariationPrice = jQuery(this.options[this.selectedIndex]).data('variation-price');

        jQuery(addToCart).data('item-price', selectVariationPrice);

    });

    var addToCart = jQuery(".add-to-cart");
// We don't need the href to take the user to the listing so we will remove it.
    jQuery(addToCart).removeAttr("href");
    jQuery(addToCart).html('ADD TO CART');

    var jQueryBody = jQuery('body');

    jQueryBody.on('click', '.add-to-cart', function (e) {

        var listingId = jQuery(this).data('listingId');
        var itemName = jQuery(this).data('itemName');
        var itemImage = jQuery(this).data('itemImage');
        var itemPrice = jQuery(this).data('itemPrice');

        var currencyPlacement = jQuery(this).data('currencyPlacement');
        var currencySymbol = jQuery(this).data('currencySymbol');
        var currencyCode = jQuery(this).data('currencyCode');

        if(currencyPlacement == 'before') {
            itemPrice = currencySymbol + jQuery(this).data('itemPrice') + ' ' + currencyCode;
        } else {
            itemPrice = jQuery(this).data('itemPrice') + currencySymbol + ' ' + currencyCode;
        }

        var itemQty = jQuery("select[name=itemQty]").val();

        var variations = {};
        var variationProperty;
        var variationPriceDiff;

        itemFound = false;

        if (jQuery('[id^=variations] [value="0"]:selected').length > 0) {
            alert('Please select from all the available item options.');
            return false;
        }

        jQuery.each(jQuery(".variations option:selected"), function () {
            variationProperty = jQuery(this).data('variationProperty');
            variations[variationProperty] = jQuery(this).val();
        });

        // Let's make sure that we don't already have the same item in the cart so we don't have two of the same item.
        jQuery("#etsy360-pop-cart-table [data-listing-id=" + listingId + "]").each(function (e) {
            itemFound = true;
        });

        jQuery("#etsy360-pop-cart-table [data-price-diff=" + variationPriceDiff + "]").each(function (e) {
            itemFound = true;
        });


        if (itemFound) {

            jQuery('.cart-alert-message').fadeIn().html('Item has already been added').delay(2000).fadeOut();

        } else {

            var itemBubble = jQuery(".itemCount");
            var itemcount = itemBubble.data('itemCount');
            var addToCount = itemcount + 1;

            jQuery('.e360-pop-windowclose').removeClass('down');
            jQuery('.e360-pop-window').removeClass('down');

            itemBubble.show();
            itemBubble.data('itemCount', addToCount);
            itemBubble.html(addToCount);

            jQuery("#etsy360-cart-table tbody:empty");
            jQuery('.cart-empty-message').remove();
            jQuery('.cart-alert-message').fadeIn().html('Item added to cart');

            var addRow = '<tr data-listing-id="' + listingId + '" data-price-diff="' + variationPriceDiff + '" class="cart-table">' +
                '<td class="cart-item-remove"><a class="remove-cart-item" data-listing-id="' + listingId + '" href="">x</a></td>' +
                '<td class="cart-item-image" style="min-width: 50px"><img src="' + itemImage + '"/></td>' +
                '<td class="cart-item-title">' + itemName + '</td>' +
                '<td class="cart-item-price">' + itemPrice + '</td>' +
                '</tr>';

            jQuery('tbody:first', '#etsy360-cart-table, #etsy360-pop-cart-table').after(addRow);

            if (jQuery.isEmptyObject(variations)) {
                variations = null;
            }

            var data = {
                'action': 'add_to_cart',
                'listingId': listingId,
                'qty': itemQty,
                'variations': variations
            };

            //console.log('Data ' + JSON.stringify(data));

            // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
            jQuery.post(ajaxurl, data, function () {
                jQuery(".cart-alert-message").fadeOut();
                //console.log('Data Inside AJAX Post : ' + JSON.stringify(data));
            });

        }

        e.preventDefault();
        return false;

    });


    jQueryBody.on('click', '.remove-cart-item', function (e) {

        var listingId = jQuery(this).data('listingId');

        jQuery('.cart-alert-message').fadeIn().html('Item removed');

        console.log('Remove Listing Id: ' + listingId);

        var data = {
            'action': 'remove_from_cart',
            'listingId': listingId
        };

        var itemBubble = jQuery(".itemCount");
        var itemcount = itemBubble.data('itemCount');
        var addToCount = itemcount - 1;

        if (addToCount < 1) {
            itemBubble.hide();
        }

        itemBubble.data('itemCount', addToCount);
        itemBubble.html(addToCount);

        // remove the table row
        jQuery(this).closest('tr').remove();

        jQuery.post(ajaxurl, data, function (response) {
            jQuery(".cart-alert-message").fadeOut();
            console.log(response);
        });

        e.preventDefault();
        return false;

    });


})
;
jQuery(function(){

    var appendthis =  ("<div class='e360-modal-overlay js-modal-close'></div>");

    jQuery('a[data-modal-id]').click(function(e) {
        e.preventDefault();
        jQuery('body').css('overflow', 'hidden');
        jQuery("body").append(appendthis);
        jQuery(".e360-modal-overlay").fadeTo(500, 0.7);
        jQuery(".js-modalbox").fadeIn(500);
        var modalBox = jQuery(this).attr('data-modal-id');
        jQuery('#'+modalBox).show();
    });

    jQuery(".js-modal-close, .e360-modal-overlay").click(function() {
        jQuery(".e360-modal-box, .e360-modal-overlay").fadeOut(500, function() {
            jQuery(".e360-modal-overlay").remove();
            jQuery('body').css('overflow', 'visible');

        });

    });

    jQuery(window).resize(function() {
        jQuery(".e360-modal-box").css({
            top: (jQuery(window).height() - jQuery(".e360-modal-box").outerHeight()) / 2
            //left: (jQuery(window).width() - jQuery(".e360-modal-box").outerWidth()) / 2
        });
    });

    //jQuery(window).resize();

});

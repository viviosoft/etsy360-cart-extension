<?php

ob_start();


class Shortcode_e360_Modal extends Etsy360_Cart_Helpers
{

    public function __construct()
    {
        add_shortcode('e360_modal', array($this, 'etsy360_add_modal_shortcode'));
    }

    function etsy360_add_modal_shortcode()
    {

        $modal = '

    <div id="animatedModal">
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID  class="close-animatedModal" -->
        <div class="close-animatedModal">
            CLOSE MODAL
        </div>

        <div class="modal-content">
                  <!--Your modal content goes here-->
        </div>
    </div>

    ';

        return $modal;

    }

}

$e360_modal = new Shortcode_e360_Modal;
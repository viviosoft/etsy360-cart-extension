<?php

ob_start();


class Shortcode_e360_Shop_Cart extends Etsy360_Cart_Helpers
{

    public function __construct()
    {
        add_shortcode('e360_cart', array($this, 'etsy360_add_shop_cart_shortcode'));
    }

    function etsy360_add_shop_cart_shortcode()
    {

        $cart = new shopping_cart_class();

        $guestCart = $cart->findGuestCart();
        $guestCheckoutLink = $cart->guestCheckoutLink();

        $content = '';

        $content .= '<table id="etsy360-cart-table">';
        $content .= '
                    <thead>
                        <tr class="cart-table">
                            <th style="width: 10%"></th>
                            <th style="width: 10%">Image</th>
                            <th style="width: 60%">Product</th>
                            <th style="width: 20%">Price</th>
                        </tr>
                        </thead><tbody>
                    ';

        if (is_array($guestCart->results[0]->listings)) :

            foreach ($guestCart->results[0]->listings as $cartItems) :

                $shopCurrency = $this->getCurrencyInfo($cartItems->currency_code);

                $price = ($shopCurrency['placement'] == 'before') ? $shopCurrency['symbol'] . $cartItems->price . " " . $cartItems->currency_code : $cartItems->price . " " . $shopCurrency['symbol'] . " " . $cartItems->currency_code;

                $content .= '<tr class="cart-table">';
                $content .= '<td class="cart-item-remove" >';
                $content .= '<a data-listing-id="' . $cartItems->listing_id . '" class="remove-cart-item" data-listing-id="' . $cartItems->listing_id . '" href="">x</a>';
                $content .= '</td>';
                $content .= '<td class="cart-item-image" style="min-width: 40px">';
                $content .= '<img class="cart-image" src="' . $cartItems->image_url_75x75 . '">';
                $content .= '</td>';
                $content .= '<td class="cart-item-title">';
                $content .= $cartItems->title;
                $content .= '</td>';
                $content .= '<td class="cart-item-price">';
                $content .= $price;
                $content .= '</td>';
                $content .= '</tr>';

            endforeach;

        endif;

        $content .= '</tbody></table>';
        $content .= '<a target="_blank" href="' . $guestCheckoutLink . '" style="background-color: ' . $this->get_wp_cart_option('accent_color') . '" class="e360-cart-btn shopDetailsButton">' . __('Checkout on Etsy') . '</a>';
        $content .= '<br />';

        return $content;
    }

}

$e360_shop_cart_shortcode = new Shortcode_e360_Shop_Cart;
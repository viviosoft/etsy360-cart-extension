<?php

class shopping_cart_class extends Etsy360_Cart_Helpers
{
//    private static $instance;
    var $guest_id = "";

    function __construct()
    {

        session_start();

        if (empty($_SESSION['GUEST_ID'])) {
            $_SESSION['GUEST_ID'] = $this->generateGuestId();
            $this->guest_id = $_SESSION['GUEST_ID'];

            if ($this->get_wp_cart_option('debugging') == 1) {
                $debug = new PHPCartDebugger();
                $debug->var2console($this->guest_id, 'GUEST ID API ', true);
            }

        } else if (!empty($_SESSION['GUEST_ID'])) {

            if ($this->get_wp_cart_option('debugging') == 1) {
                $debug = new PHPCartDebugger();
                $debug->var2console($_SESSION['GUEST_ID'], 'GUEST ID SESSION ', true);
            }

            $this->guest_id = $_SESSION['GUEST_ID'];
        }

    }

    function getUserShop()
    {

        $user = $this->_getCartRequest("https://openapi.etsy.com/v2/users/__SELF__", 'GET');
        return $user;

    }

    function removeGuestCartListing($listingId)
    {

        $removeFromCart = $this->_getCartRequest("https://openapi.etsy.com/v2/guests/$this->guest_id/carts/?listing_id=$listingId", 'DELETE');
        return $removeFromCart;

    }

    function addToGuestCart($listingId, $qty, $variations)
    {

        $variations_uri = ($variations == 'null') ? "" : "&selected_variations=$variations";

        $addToCart = $this->_getCartRequest("https://openapi.etsy.com/v2/guests/$this->guest_id/carts/?listing_id=$listingId&quantity=$qty$variations_uri", 'POST');
        return $addToCart;

    }

    function findGuestCart()
    {

        $guestCart = $this->_getCartRequest("https://openapi.etsy.com/v2/guests/$this->guest_id/carts", 'GET');
        return $guestCart;

    }

    function guestCheckoutLink()
    {

        $guestCartLink = $this->_getCartRequest("https://openapi.etsy.com/v2/guests/$this->guest_id", 'GET');
        return $guestCartLink->results[0]->checkout_url;

    }

    public function generateGuestId()
    {

        $guestCartId = $this->_getCartRequest('https://openapi.etsy.com/v2/guests/generator', 'GET');
        return $guestCartId->results[0]->guest_id;

    }

    private function _getCartRequest($uri, $requestType)
    {

        if (class_exists('Etsy360OAuth\Etsy360_oAuth')) {

            if ($success = oauth_etsy_class()->Initialize()) {
                oauth_etsy_class()->CallAPI(
                    $uri,
                    $requestType, array(), array('FailOnAccessError' => true), $returnRequest);
                $success = oauth_etsy_class()->Finalize($success);
            }

            if ($success) {

                if ($this->get_wp_cart_option('debugging') == 1) {
                    $debug = new PHPCartDebugger();
                    $debug->var2console($returnRequest, 'API SUCCESS ', true);
                }

                return $returnRequest;

            } else {

                if ($this->get_wp_cart_option('debugging') == 1) {
                    $debug = new PHPCartDebugger();
                    $debug->var2console($returnRequest, 'API ERROR : ', true);
                }
                return $returnRequest;
            }

        } else {

            $debug = new PHPCartDebugger();
            $debug->var2console('OAuth Plugin not enabled', ' ', true);

            return true;
        }

    }

}
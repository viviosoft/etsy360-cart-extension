<?php

/**
 * WordPress Console Debug
 *
 * Debug PHP using the JavaScript console.
 * Enable this plugin and view your console for more information on how to use it.
 *
 * @package   WP_console_debug
 * @author    DRSK
 * @license   WTFPL
 *
 * @wordpress-plugin
 * Plugin Name: WordPress Console Debug
 * Description: Debug PHP using the JavaScript console. View your console after activation for more details.
 * Version:     1.0
 * Author:      DRSK
 */

// If this file is called directly
if (!defined('WP_PLUGIN_URL') || !defined('WPINC'))
    die('Restricted access');

// WP_console_debug
class PHPCartDebugger
{

    /**
     * Logs messages/variables/data to browser console from within php
     *
     * @param $name : message to be shown for optional data/vars
     * @param $data : variable (scalar/mixed) arrays/objects, etc to be logged
     * @param $jsEval : whether to apply JS eval() to arrays/objects
     *
     * @return none
     * @author Sarfraz
     */

    function var2console($var, $name = '', $now = false)
    {
        if ($var === null) $type = 'NULL';
        else if (is_bool($var)) $type = 'BOOL';
        else if (is_string($var)) $type = 'STRING[' . strlen($var) . ']';
        else if (is_int($var)) $type = 'INT';
        else if (is_float($var)) $type = 'FLOAT';
        else if (is_array($var)) $type = 'ARRAY[' . count($var) . ']';
        else if (is_object($var)) $type = 'OBJECT';
        else if (is_resource($var)) $type = 'RESOURCE';
        else                        $type = '???';
        if (strlen($name)) {
            $this->str2console("$type $name = " . var_export($var, true) . ';', $now);
        } else {
            $this->str2console("$type = " . var_export($var, true) . ';', $now);
        }
    }

    function str2console($str, $now = false)
    {
        if ($now) {
            echo "<script type='text/javascript'>\n";
            echo "//<![CDATA[\n";
            echo "console.log(", json_encode($str), ");\n";
            echo "//]]>\n";
            echo "</script>";
        } else {
            register_shutdown_function('str2console', $str, true);
        }
    }


    function debugArr($var)
    {

        echo '<pre>';
        echo print_r($var);
        echo '</pre>';

    }


}


// create new instance of the class
$wp_console_debug = new PHPCartDebugger();
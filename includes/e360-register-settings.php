<?php

//ob_start();
//
//function e360_cart_connect_oauth()
//{
//
//    if ($_GET['oauth_token']) {
//
//        // Need to call this operation again to store the new access token and secret into db.
//        if ($success = oauth_etsy_class()->Initialize()) {
//            if ($success = oauth_etsy_class()->Process()) {
//            }
//        }
//        $url = site_url() . '/wp-admin/admin.php?page=etsy360-options&tab=api&oauth_success=true';
//        wp_redirect($url);
//
//    }
//
//    if ($_GET['oauth_success']) {
//
//        if ($success = oauth_etsy_class()->Initialize()) {
//            if ($success = oauth_etsy_class()->Process()) {
//            }
//        }
//    }
//
//    // Called when connect button is clicked
//    if (isset($_POST['connect_oauth'])) {
//
//        if ($success = oauth_etsy_class()->Initialize()) {
//            if ($success = oauth_etsy_class()->Process()) {
//            }
//        }
//        exit;
//
//    }
//
//    if (isset($_POST['reset_oauth'])) {
//        global $wpdb;
//        $table_name = $wpdb->prefix . 'etsy_oauth';
//
//        $wpdb->query('TRUNCATE TABLE ' . $table_name . '');
//
//        unset($_SESSION['OAUTH_ACCESS_TOKEN']);
//    }
//
//}
//
//add_action('admin_init', 'e360_cart_connect_oauth', 1);
//
//function e360_oauth_connect_callback($args)
//{
//    global $wpdb;
//    $table_name = $wpdb->prefix . 'etsy_oauth';
//    $db_results = $wpdb->get_row("SELECT id, session, state, access_token, access_token_secret, expiry, authorized, type, server, creation, user FROM " . $table_name . " WHERE user = '1'");
//
//    if (!empty($db_results)) {
//
//        $html = '<img style="vertical-align: middle;width: 23px;padding-bottom: 5px; margin: 0 2px;" src="' . plugin_dir_url(__FILE__) . 'imgs/check-mark.png"> OAuth Connected! ';
//        $html .= '<input type="submit" class="button-secondary" id="reset_oauth" name="reset_oauth" value="' . __('Reset') . '"/>';
//
//    } else {
//
//        $e360_option = get_option('e360_settings');
//
//        if (!empty($e360_option['api_secret']) && !empty($e360_option['etsy_api_key'])) {
//            $html = '<input type="submit" class="button-secondary" name="' . $args['id'] . '" value="' . __('Connect') . '"/>';
//        }
//    }
//
//
//    echo $html;
//}
//
function e360_cart_settings($settings)
{

    $cart_style_setting = array(

        array(
            'id' => 'cart_settings_header',
            'name' => '<strong style="font-size: 18px;">' . __('Popup Cart Settings') . '</strong>',
            'desc' => '',
            'type' => 'header'
        ),
        array(
            'id' => 'cart_position',
            'name' => __('Cart Position'),
            'desc' => __('Where would you like the cart to be placed on the bottom of the screen? Left or Right? Default is Right.'),
            'std' => get_option('cart_position'),
            'type' => 'select',
            'options' => array(
                'right' => __('Right'),
                'left' => __('Left')
            )
        ),
//        array(
//            'id' => 'cart_show',
//            'name' => __('Show Cart'),
//            'desc' => __('Where would you like the cart to be placed on the bottom of the screen? Left or Right? Default is Right.'),
//            'std' => get_option('cart_position'),
//            'type' => 'select',
//            'options' => array(
//                'right' => __('Right'),
//                'left' => __('Left')
//            )
//        )
    );

    return array_merge($settings, $cart_style_setting);

}

add_filter('e360_settings_styles', 'e360_cart_settings');
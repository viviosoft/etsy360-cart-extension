<?php

function load_e360_cart_scripts()
{

    wp_register_script('general-cart-scripts', E360_CART_PLUGIN_URL . 'assets/js/etsy360-cart-scripts.js', array('jquery'));

//    wp_register_script('facebook-scripts', '//connect.facebook.net/en_US/all.js');

    wp_register_style('plugin-cart-styles', E360_CART_PLUGIN_URL . 'assets/css/etsy360-cart-styles.css', array(), '1.0');

//    wp_register_script('modal-scripts', E360_CART_PLUGIN_URL . 'assets/js/animatedModal.js', array('jquery'));
//    wp_register_style('modal-normalize-styles', E360_CART_PLUGIN_URL . 'assets/css/normalize.min.css', array(), '1.0');


    wp_enqueue_script('general-cart-scripts');
    wp_enqueue_script('modal-scripts');
//    wp_enqueue_script('facebook-scripts');

    wp_enqueue_style('plugin-cart-styles');
//    wp_enqueue_style('modal-animate-styles');
//    wp_enqueue_style('modal-normalize-styles');

}
<?php


add_action('widgets_init', create_function('', 'return register_widget("Etsy360_Shop_Cart_widget");'));

class Etsy360_Shop_Cart_widget extends WP_Widget
{

    /** constructor -- name this the same as the class above */
    function Etsy360_Shop_Cart_widget()
    {
        parent::WP_Widget(false, $name = 'Etsy360 Shopping Cart');
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance)
    {
        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
//        $page = apply_filters('page_name', $instance['page']);

        ?>
        <?php echo $before_widget; ?>
        <?php

        echo '<h3 class="widget-title">' . $title . '</h3>';

        $is_cart_widget = true;
        $content = '';

        echo do_shortcode('[e360_cart]');


        ?>



        <?php echo $after_widget; ?>
    <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
//        $instance['page'] = strip_tags($new_instance['page']);
        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance)
    {

        $title = esc_attr($instance['title']);
//        $page = esc_attr($instance['page']);

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/>
        </p>

    <?php
    }


} // end class

?>
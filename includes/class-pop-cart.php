<?php

ob_start();

class e360_Pop_Cart extends Etsy360_Cart_Helpers
{

    public function __construct()
    {
        add_action('admin_init', array($this, 'apply_checkout_button'));
        add_action('admin_init', array($this, 'etsy360_add_pop_cart'));
    }

    function etsy360_add_pop_cart()
    {

//        $content = '<div id="fb-root"></div>';

        if (!is_admin()) {

            $cart = new shopping_cart_class();

            $guestCart = $cart->findGuestCart();
            $guestCheckoutLink = $cart->guestCheckoutLink();

            $itemCount = count($guestCart->results[0]->listings);

            $cartPosition = ($this->get_wp_cart_option('cart_position') == 'left') ? 'Left: 20px;' : '';

            $content .= '<div class="e360-pop-window down" style="display: none; ' . $cartPosition . '">';
            $content .= '<div class="e360-pop-headline" style="background: ' . $this->get_wp_cart_option('accent_color') . '"><a target="_blank" href="' . $guestCheckoutLink . '"><div class="e360-cart-icon"></div></a> Cart <div class="itemCount" data-item-count="' . $itemCount . '">' . $itemCount . '</div><div class="e360-pop-windowclose down" id="e360-pop-windowclose"></div></div>';
            $content .= '<div class="e360-pop-container">';
            $content .= '<div class="e360-pop-holder">';
            $content .= '<table id="etsy360-pop-cart-table">';
            $content .= '<thead>
                        <tr class="cart-table">
                            <th style="width: 10%"></th>
                            <th style="width: 15%">Image</th>
                            <th style="width: 65%">Product</th>
                            <th style="width: 20%">Price</th>
                        </tr>
                        </thead><tbody>
                    ';

            if (is_array($guestCart->results[0]->listings)) :

                foreach ($guestCart->results[0]->listings as $cartItems) :

                    $shopCurrency = $this->getCurrencyInfo($cartItems->currency_code);
                    $price = ($shopCurrency['placement'] == 'before') ? $shopCurrency['symbol'] . $cartItems->price . " " . $cartItems->currency_code : $cartItems->price . " " . $shopCurrency['symbol'] . " " . $cartItems->currency_code;

                    $content .= '<tr class="cart-table">';
                    $content .= '<td data-listing-id="'. $cartItems->listing_id .'" class="cart-item-remove" >';
                    $content .= '<a data-listing-id="' . $cartItems->listing_id . '" class="remove-cart-item" data-listing-id="' . $cartItems->listing_id . '" href="">x</a>';
                    $content .= '</td>';
                    $content .= '<td class="cart-item-image" style="min-width: 40px">';
                    $content .= '<img src="' . $cartItems->image_url_75x75 . '">';
                    $content .= '</td>';
                    $content .= '<td class="cart-item-title">';
                    $content .= $cartItems->title;
                    $content .= '</td>';
                    $content .= '<td class="cart-item-price">';
                    $content .= $price;
                    $content .= '</td>';
                    $content .= '</tr>';

                endforeach;

            endif;

            $content .= '</tbody></table>';

            $content .= $this->apply_checkout_button($guestCheckoutLink);

            $content .= '</div>';
            $content .= '</div>';
            $content .= '</div>';

//            $content .= '<div id="popup1" class="e360-modal-box" style="display: none;">
//                          <header> <a href="#" class="js-modal-close close">×</a>
//                            <h3>Pop Up One</h3>
//                          </header>
//                          <div class="e360-modal-body">
//                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut commodo at felis vitae facilisis. Cras volutpat fringilla nunc vitae hendrerit. Donec porta id augue quis sodales. Sed sit amet metus ornare, mattis sem at, dignissim arcu. Cras rhoncus ornare mollis. Ut tempor augue mi, sed luctus neque luctus non. Vestibulum mollis tristique blandit. Aenean condimentum in leo ac feugiat. Sed posuere, est at eleifend suscipit, erat ante pretium turpis, eget semper ex risus nec dolor. Etiam pellentesque nulla neque, ut ullamcorper purus facilisis at. Nam imperdiet arcu felis, eu placerat risus dapibus sit amet. Praesent at justo at lectus scelerisque mollis. Mauris molestie mattis tellus ut facilisis. Sed vel ligula ornare, posuere velit ornare, consectetur erat.</p>
//                            <p>
//
//<div class="fb-share-button"
//		data-href="http://www.your-domain.com/your-page.html"
//		data-layout="button_count">
//	</div>
//</p>
//                          </div>
//                          <footer> <a href="#" class="e360-modal-btn e360-modal-btn-small js-modal-close">Close</a> </footer>
//                    </div>
//       ';

            return apply_filters('e360_popup_cart', $content);


        } else {

            return $content;

        }

    }

    /***
     * @param $guestCheckoutLink
     * @return mixed
     * Used to setup the button filter so other plugins can overwrite if needed
     */
    function apply_checkout_button($guestCheckoutLink)
    {

        return apply_filters('e360_checkout_btn',
            '<a target="_blank" href="' . $guestCheckoutLink . '" style="background-color: ' . $this->get_wp_cart_option('accent_color') . '" class="e360-cart-btn checkoutButton">' . __('Checkout on Etsy') . '</a>');

    }

}

//function update_checkout_btn($args)
//{
//
//    $helper = new Etsy360_Cart_Helpers();
//    $cart = new shopping_cart_class();
//
//    $guestCheckoutLink = $cart->guestCheckoutLink();
////    $new = '<a id="openModal" target="_blank" href="' . $guestCheckoutLink . '" style="background-color: ' . $helper->get_wp_cart_option('accent_color') . '" class="e360-cart-btn checkoutButton">' . __('Checkout on Etsy') . '</a>';
//    $new = '<a data-modal-id="popup1" style="background-color: ' . $helper->get_wp_cart_option('accent_color') . '" class="e360-cart-btn checkoutButton">' . __('Checkout on Etsy') . '</a>';
//
//    return $new;
//}
//
//add_filter('e360_checkout_btn', 'update_checkout_btn');

//function add_meta_tags()
//{
//
//    echo '<meta property="og:url" content="http://www.nytimes.com/2015/02/19/arts/international/when-great-minds-dont-think-alike.html"/>' . "\n";
//    echo '<meta property="og:type" content="article" />' . "\n";
//    echo '<meta property="og:title" content="When Great Minds Don’t Think Alike" />' . "\n";
//    echo '<meta property="og:description" content="How much does culture influence creative thinking?" />' . "\n";
//    echo '<meta property="og:image" content="http://static01.nyt.com/images/2015/02/19/arts/international/19iht-btnumbers19A/19iht-btnumbers19A-facebookJumbo-v2.jpg" />' . "\n";
//}
//
//add_action('wp_head', 'add_meta_tags', 2);
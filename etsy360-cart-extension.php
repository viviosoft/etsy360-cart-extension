<?php
/**
 * Plugin Name: Etsy360 Cart Extension
 * Plugin URI: http://www.etsy360.com
 * Description: The Etsy360 Shopping Cart Extension allows your visitors to add your Etsy items to a cart and then checkout on the Etsy Website.
 * Author: VivioSoft
 * Author URI: http://www.viviosoft.com
 * Version: 2.0.3
 * Requires at least: 3.6
 * Tested up to: 4.6
 * Text Domain: Etsy360 Cart Extension
 * @package Etsy360 Cart Extension
 */

/*
 *
 */

// Exit if accessed directly
if (!defined('ABSPATH')) exit;

if (!class_exists('Etsy360Cart')) :

    /**
     * Main Class
     */
    final class Etsy360Cart
    {

        private static $cart_instance;
        public $createGuest;

        public static function cart_instance()
        {
            if (!isset(self::$cart_instance) && !(self::$cart_instance instanceof Etsy360Cart)) {

                self::$cart_instance = new Etsy360Cart;

                self::$cart_instance->setup_cart_constants();
                self::$cart_instance->include_cart_files();

                add_action('admin_init', array(__CLASS__, 'e360_cart_updater'), 1);
//                add_action('template_redirect', array(__CLASS__, 'load_pop_cart_function'), 1, 1);
                add_action('wp_head', array(__CLASS__, 'load_pop_cart_function'), 12);

                add_action('admin_notices', array(__CLASS__, 'e360_extended_exists'));

                add_action('wp_head', array(__CLASS__, 'e360_ajaxurl'));
                // This is a fix for IE and FireFox browsers
                add_action('wp_ajax_nopriv_add_to_cart', array(__CLASS__, 'add_to_cart_callback'));
                add_action('wp_ajax_add_to_cart', array(__CLASS__, 'add_to_cart_callback'));

                add_action('wp_ajax_nopriv_remove_from_cart', array(__CLASS__, 'remove_from_cart_callback'));
                add_action('wp_ajax_remove_from_cart', array(__CLASS__, 'remove_from_cart_callback'));

                add_action('init', 'load_e360_cart_scripts');


            }
            return self::$cart_instance;
        }

        /**
         * Include required files
         *
         * @access private
         * @since 1.4
         * @return void
         */
        static function include_cart_files()
        {

            require_once E360_CART_PLUGIN_DIR . 'includes/class-debugger.php';
            require_once E360_CART_PLUGIN_DIR . 'includes/etsy360-cart-scripts.php';
            require_once E360_CART_PLUGIN_DIR . 'includes/class-helpers.php';

            if (class_exists('Etsy360OAuth\Etsy360_oAuth')) {
                require_once E360_CART_PLUGIN_DIR . 'includes/shopping_cart_class.php';
            }

            require_once E360_CART_PLUGIN_DIR . 'includes/class-shop-cart-widget.php';
            require_once E360_CART_PLUGIN_DIR . 'includes/class-shop-cart-shortcode.php';

//            require_once E360_CART_PLUGIN_DIR . 'includes/class-modal-shortcode.php';

            require_once E360_CART_PLUGIN_DIR . 'includes/class-pop-cart.php';

            if (is_admin()) {
                require_once E360_CART_PLUGIN_DIR . 'includes/e360-register-settings.php';
            }

        }


        private function setup_cart_constants()
        {

            if (!defined('E360_CART_ITEM_NAME')) {
                define('E360_CART_ITEM_NAME', 'Cart Extension');
            }

            // Plugin version
            if (!defined('E360_CART_VERSION')) {
                define('E360_CART_VERSION', '2.0.3');
            }

            // Plugin Folder Path
            if (!defined('E360_CART_PLUGIN_DIR')) {
                define('E360_CART_PLUGIN_DIR', plugin_dir_path(__FILE__));
            }

            // Plugin Folder URL
            if (!defined('E360_CART_PLUGIN_URL')) {
                define('E360_CART_PLUGIN_URL', plugin_dir_url(__FILE__));
            }

            // Plugin Root File
            if (!defined('E360_CART_PLUGIN_FILE')) {
                define('E360_CART_PLUGIN_FILE', __FILE__);
            }

            // Plugin Root File
            if (!defined('E360_CART_LOADER')) {
                define('E360_CART_LOADER', '<img id="loader" class="loader" src="' . E360_CART_PLUGIN_URL . 'includes/imgs/loader.gif">');
            }


        }

        function load_pop_cart_function()
        {
            if (is_page() || is_home() || is_single()) {
                $e360_Pop_cart = new e360_Pop_Cart;
                echo $e360_Pop_cart->etsy360_add_pop_cart();
            }
        }

        function e360_ajaxurl()
        {

            ?>
            <script type="text/javascript">
                var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            </script>
            <?php
        }

        public function e360_cart_updater()
        {

            if (class_exists('E360_License')) {
                $e360_cart_license = new E360_License(__FILE__, E360_CART_ITEM_NAME, E360_CART_VERSION, 'viviosoft', 'e360_cart_license_key');
            }

        }

        function add_to_cart_callback()
        {

            $listingId = intval($_POST['listingId']);
            $qty = intval($_POST['qty']);
            $variations = json_encode($_POST['variations']);

            $cart = new shopping_cart_class();
            $addToCart = $cart->addToGuestCart($listingId, $qty, $variations);

//            $debug = new PHPCartDebugger();
//            $debug->debugArr($addToCart);

            die();

        }

        function remove_from_cart_callback()
        {
            global $wpdb;
            $listingId = intval($_POST['listingId']);

            $cart = new shopping_cart_class();

            $removeFromCart = $cart->removeGuestCartListing($listingId);

//            $debug = new PHPCartDebugger();
//            $debug->debugArr($removeFromCart);

            die();

        }

        function e360_extended_exists()
        {

            if (!class_exists('Etsy360')) {
                $url = 'https://www.etsy360.com/downloads/etsy-wordpress-shop-plugin-extended/';
                echo '<div class="admin_error"><p>';
                printf(__('Etsy360 Cart Extension needs attention. You will need the Etsy360 Extended Plugin before you can use this extension.  Make sure you have it installed and active. <a target="_blank" href="' . $url . '">Download Etsy360 Extended.</a>'), '');
                echo "</p></div>";
            }
        }

    }

endif; // End if class_exists check

function Etsy360Cart_Plugin()
{
    return Etsy360Cart::cart_instance();
}

// Get Plugin Running
Etsy360Cart_Plugin();

?>